<?php

class cv_webhook_log {

  /**
  * @var string Panel page
  */
  public function __construct() {
    add_action('wp_ajax_nopriv_webhook', array($this, 'log'));

  }

  function log(){
    global $cv_webhook;
    if (isset($_POST)){
        $cv_webhook->logMsg('', 'init');
        $cv_webhook->logMsg('Receive REQUEST - '.json_encode($_POST), 'info');
        $cv_webhook->logMsg('Receive READERS - '.json_encode(getallheaders()) , 'info');
        $cv_webhook->logMsg('', 'end');
    }
echo "ok";
wp_die();
  }


}
