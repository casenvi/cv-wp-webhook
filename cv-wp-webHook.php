<?php

/*
  Plugin Name: CV WebHook Admin
  Plugin URI: http://www.cassiovidal.com
  Description: Plugin para receber requisções do webhook e gravar em log
  Version: 1.0
  Author: Cassio Vidal
  Author URI: http://www.cassiovidal.com
  License: GPLv2
  Text Domain: cv_webHook
  Domain Path: /languages/
 */

/*
 *      Copyright 2018 Cassio Vidal <comercial@cassiovidal.com>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

// Verification of prerequisites
function admin_notice__error($message) {
    $class = 'notice notice-error';
    printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), esc_html($message));
}

if (!class_exists('cv_webHook')):

    define('CV_WH_PLUGIN_DIR_PATH', plugin_dir_path(__FILE__));
    define('CV_WH_PLUGIN_DIR_URL', plugin_dir_url(__FILE__));

    class cv_webHook {

        const VERSION = '1.0.0';

        protected static $instance = null;

        private function __construct() {
            // Load plugin text domain.
            add_action('init', array($this, 'load_plugin_textdomain'));
            //add links to adm
            add_action('admin_menu', array($this, 'cv_plugin_links'));
            global $cv_webhook;
            $cv_webhook = $this->cv_init();
        }

        public function load_plugin_textdomain() {
            load_plugin_textdomain('cv_webhook', false, 'languages/');
        }

        public static function get_instance() {
            // If the single instance hasn't been set, set it now.
            if (null === self::$instance) {
                self::$instance = new self;
            }

            return self::$instance;
        }

        /**
         * Adds plugin page links
         *
         * @param array $links all plugin links
         * @return array $links all plugin links + our custom links (i.e., "Settings")
         */
        public function cv_plugin_links() {
            // add menu-page
            add_menu_page('WebHook', 'WebHook', 'manage_options', 'cv-webHook-plugin', null,
                plugin_dir_url(__FILE__) . 'assets/images/menu-icon.png', 0);
            add_submenu_page('cv-webHook-plugin', __('Instructions', 'cv_webHook'), __('Instructions', 'cv_webHook'),
                'manage_options', 'cv-webHook-plugin', array($this, 'instruction'));
        }

        public function instruction() {
            wp_register_style('bootstrapcss', 'assets/css/bootstrap/bootstrap.css');
            wp_enqueue_style('bootstrapcss');

            include_once 'includes/frontend/adm_instruction.php';
        }


        public function cv_init() {

            include_once CV_WH_PLUGIN_DIR_PATH . 'includes/class-cv_webhook_log.php';
            $this->template = new cv_webhook_log();

            return $this;
        }

        public function install() {
            //require_once CV_WH_PLUGIN_DIR_PATH . 'assets/libs/db/install_full.php';
            //install_db();
        }
        public function logMsg($msg, $level = 'info', $file = 'general.log') {
          if (!file_exists(CV_WH_PLUGIN_DIR_PATH ."log/")) {
            mkdir(CV_WH_PLUGIN_DIR_PATH ."log/");
          }
          $file = CV_WH_PLUGIN_DIR_PATH ."log/".date('Y-m-d',current_time( 'timestamp', 0 ))."_".$file;
          $levelStr = '';
          switch ($level) {
            case 'info':
              $levelStr = 'INFO';
            break;
            case 'warning':
              $levelStr = 'WARNING';
            break;
            case 'error':
              $levelStr = 'ERROR';
            break;
          }
          $msg = sprintf("[%s] [%s]: %s%s", date( 'Y-m-d H:i:s', current_time( 'timestamp', 0 ) ), $levelStr, $msg, PHP_EOL);
          file_put_contents($file, $msg, FILE_APPEND);
        }

    }

    register_activation_hook(__FILE__, array('cv_webHook', 'install'));
    add_action('plugins_loaded', array('cv_webHook', 'get_instance'));
endif;
